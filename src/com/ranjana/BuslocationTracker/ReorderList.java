package com.ranjana.BuslocationTracker;

import java.util.Scanner;
import java.util.Stack;

class Node{
		        int data;
		        Node next;
		        Node(int d) {data = d; next = null; }
		    }
			
public class ReorderList{
		    Node head;  // head of lisl
		  
		    /* Linked list Node*/
		   
		                    
		    /* Utility functions */
		 
		    /* Inserts a new Node at front of the list. */
		     public void addToTheLast(Node node) {
		  if (head == null) {
		   head = node;
		  } else {
		   Node temp = head;
		   while (temp.next != null)
		    temp = temp.next;
		   temp.next = node;
		  }
		 }
		  /* Function to print linked list */
		    void printList()
		    {
		        Node temp = head;
		        while (temp != null)
		        {
		           System.out.print(temp.data+" ");
		           temp = temp.next;
		        }  
		        System.out.println();
		    }
			
			 
		 
		     /* Drier program to test above functions */
public static void main(String args[])
		    {
		       
		         
		        /* Constructed Linked List is 1->2->3->4->5->6->
		           7->8->8->9->null */
		         Scanner sc = new Scanner(System.in);
				 int t=sc.nextInt();
				 while(t>0)
		         {
					int n = sc.nextInt();
					ReorderList llist = new ReorderList();
					//int n=Integer.parseInt(br.readLine());
					int a1=sc.nextInt();
					Node head= new Node(a1);
		            llist.addToTheLast(head);
		            for (int i = 1; i < n; i++) {
		            int a = sc.nextInt(); 
		            llist.addToTheLast(new Node(a));
		         }
		          //int k=sc.nextInt();
		         
		        llist.head = new gfg().reorderlist(llist.head);
		        //llist.printList();
				//llist.head = llist.reverse(llist.head);
				llist.printList();
				
		        t--;
		    }
		
		}
		/*This is a function problem.You only need to complete the function given below*/
		/* Following is the Linked list node structure */
		/*class Node
		{
		    int data;
		    Node next;
		    Node(int d) {data = d; next = null; }
		}*/
			
		

}
class gfg
{
    Node reorderlist(Node head)
    {
        // Your code here
        
        Node first = head,second = head;
        while(second != null && second.next != null){
            first = first.next;
            second = second.next.next;
        }
    
    Node x = first.next;
    first.next = null;
        Stack<Integer> stack = new Stack<Integer>();
        while(x != null){
            stack.push(x.data);
            x = x.next;
        }
        Node temp2 = head;
        Node add;
        while(!stack.isEmpty()){
            add = new Node(stack.peek());
            stack.pop();
            add.next = temp2.next;
            temp2.next =add;
            temp2=add.next;
          
        }
        return head;
    }
}
