package com.ranjana.BuslocationTracker;

import java.util.Scanner;

class Node1
		{
		    int data;
		    Node1 next;
		    Node1(int d) {
		        data = d; 
		        next = null;
		    }
		}
class Mergelist
		{
	Node1 head;
		    public void addToTheLast(Node1 node)
		    {
		      if (head == null) {
		           head = node;
		          } else {
		        	  Node1 temp = head;
		           while (temp.next != null)
		            temp = temp.next;
		        
		           temp.next = node;
		        }
		    }
		  /* Function to print linked list */
		    void printList()
		    {
		    	Node1 temp = head;
		        while (temp != null)
		        {
		           System.out.print(temp.data+" ");
		           temp = temp.next;
		        }  
		        System.out.println();
		    }
			
			 
		 
		     /* Driver program to test above functions */
		    public static void main(String args[])
		    {
		       
		         
		        /* Constructed Linked List is 1->2->3->4->5->6->
		           7->8->8->9->null */
		         Scanner sc = new Scanner(System.in);
				 int t=sc.nextInt();
				 
				 while(t>0)
		         {
					int n1 = sc.nextInt();
					int n2 = sc.nextInt();
					Mergelist llist1 = new Mergelist();
					Mergelist llist2 = new Mergelist();
					
						int a1=sc.nextInt();
						Node1 head1= new Node1(a1);
						llist1.addToTheLast(head1);
						if (sc.hasNextInt())
						for (int i = 1; i < n1; i++) 
						{ 
							//int a;
		 
							 int a = sc.nextInt(); 
							llist1.addToTheLast(new Node1(a));
						}
					
					
						int b1=sc.nextInt();
						Node1 head2 = new Node1(b1);
						llist2.addToTheLast(head2);
						if (sc.hasNextInt()) 
						for (int i = 1; i < n2; i++) 
						{
							//int b;
							
							 int b = sc.nextInt(); 
							llist2.addToTheLast(new Node1(b));
						}
						llist1.head= new LinkedList().sortedMerge(llist1.head,llist2.head);
						llist1.printList();
					
					t--;
					
		         }
		    }
		}
		/*This is a function problem.You only need to complete the function given below*/
		/*
		  Merge two linked lists 
		  head pointer input could be NULL as well for empty list
		  Node is defined as 
		    class Node
		    {
		        int data;
		        Node next;
		        Node(int d) {data = d; next = null; }
		    }
		*/
class LinkedList{
	Node1 sortedMerge(Node1 A, Node1 B) {
		     // This is a "method-only" submission. 
		     // You only need to complete this method
		Node1 head ,dummy;
		         head = new Node1(0);
		         Node1 curr= head;
		        while(A != null && B != null){
		            if(A.data > B.data){
		                dummy = new Node1(B.data);
		                B=B.next;
		            }else{
		                dummy = new Node1(A.data);
		                A= A.next;
		            }
		            curr.data = dummy.data;
		            curr = curr.next;
		        }
		        
		        while(A != null){
		            dummy = new Node1(A.data);
		            A= A.next;
		            curr.data = dummy.data;
		            curr = curr.next;
		        }
		        
		         while(B != null){
		            dummy = new Node1(B.data);
		            B= B.next;
		            curr.data = dummy.data;
		            curr = curr.next;
		        }
		        return head.next;
		   } 
		}

