package interview_questions;

import java.util.HashMap;

//quetions Find the most frequent integer in an array

public class MostFrequestIntegerInArray {
	public static void main(String args[]) {
		   int arr[] = {1,2,3,3,5,6,7,8,5,5, 5, 2, 1, 3, 2, 1}; 	          
	        System.out.println(mostFreq(arr)); 
	          
	    }

	private static int mostFreq(int[] arr) {
        int n = arr.length; 
        int max = arr[0];
        HashMap<Integer,Integer> h = new HashMap<>();
        for(int i =0;i<n;i++) {
        	if(h.containsKey(arr[i])) {
            	h.put(arr[i], h.get(arr[i])+1);
        	}else {
            	h.put(arr[i], 1);
        	}
        	
        	if(h.get(arr[i]) > h.get(max)) {
        		max = arr[i];
        	}
        }
        return max;
        
	}
}
