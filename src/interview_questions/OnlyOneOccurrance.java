package interview_questions;

import java.util.HashMap;

//Find the only element in an array that only occurs once.
public class OnlyOneOccurrance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		   int a[] = {1,2,2,3,3,5,5,6,6,7,7,8,8}; 
	        System.out.println(checkOccurance(a)); 
	          
	    }

	private static int checkOccurance(int[] a) {
		 HashMap<Integer,Integer> h = new HashMap<>();
	        for(int i =0;i<a.length;i++) {
	        	if(h.containsKey(a[i])) {
	            	h.put(a[i], h.get(a[i])+1);
	        	}else {
	            	h.put(a[i], 1);
	        	}
	        }
	        	  for(int i =0;i<a.length;i++) {
	  	        	if(h.get(a[i]) ==1) {
	  	            	return a[i];
	  	        	}
	        	  }
        	return 0;
	}
	
}
