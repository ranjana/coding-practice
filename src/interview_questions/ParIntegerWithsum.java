package interview_questions;

import java.util.ArrayList;
import java.util.HashMap;

public class ParIntegerWithsum {
	public static void main(String args[]) {
		   int arr[] = {1,2,3,3,5,6,7,8,5,5, 5, 2, 1, 3, 2, 1}; 	          
	       System.out.println(parInteger(arr)); 
	          
	    }

	private static int parInteger(int[] arr) {
		// TODO Auto-generated method stub
		HashMap<Integer,Integer> h = new HashMap<>();
		int count = 0;
		int sum =10;
		for(int i =0;i<arr.length;i++) {
			if(h.containsKey(arr[i])) {
				h.put(arr[i], h.get(arr[i]));
			}else {
				h.put(arr[i], 1);
			}
		}
		for(int i =0;i<arr.length;i++) {
			if(h.get(sum-arr[i]) != null) {
                count += h.get(sum-arr[i]); 
			}
            if (sum-arr[i] == arr[i]) 
                count--; 
		}
		return (int)(count/2);
	}
}
