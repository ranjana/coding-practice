package interview_questions;


//Given 2 integer arrays, determine if the 2nd array is a rotated version of the 1st array.
//
//Ex. Original Array A={1,2,3,5,6,7,8} Rotated Array B={5,6,7,8,1,2,3}
public class chackRotatedArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		   int a[] = {1,2,3,5,6,7,8}; 
		   int b[] = {5,6,7,8,1,2,3}; 
	        System.out.println(checkRotated(a,b)); 
	          
	    }

	private static boolean checkRotated(int[] a, int[] b) {
		if(a.length != b.length) {
			return false;
		}
		int startA = 0;
		boolean gotmatch = false;
		int j =0;
			for(j =0;j<b.length ;j++) {
				if(a[startA] == b[j]) {
					gotmatch = true;
					break;
				}
			}
			if(!gotmatch) {
				return false;
			}
		
			while(startA<a.length) {
				if(a[startA] != b[j]) {
					return false;
				}
				if(j == b.length-1) {
					j=0;
				}else {
					j++;
				}
				startA++;	
			}
			
		return true;
	}

	}
