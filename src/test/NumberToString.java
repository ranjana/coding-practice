package test;

import java.util.Scanner;

public class NumberToString {
	public static void main (String[] args)
	 {
		 Scanner sc = new Scanner(System.in);
		 int t = sc.nextInt();
		 while(t-->0){
		     String str = sc.next();
		     System.out.print(getsum(str));
		    }
		 }
		 static int getsum(String str){
		     int min = Integer.MAX_VALUE;
		     String num ="";
		     for(int i = 0;i<str.length();i++){
		         char c = str.charAt(i);
		         if(c > 96 && c <123){
		        	 if(num != "") {
		        	 min = Integer.parseInt(num) < min?Integer.parseInt(num) :min;
		             num="";
		        	 }
		         }else{
		             num = num+String.valueOf(str.charAt(i));
		             
		         }
		 }
		     
		     if(num != "") {
		        min = Integer.parseInt(num) < min  ?  Integer.parseInt(num) :min;
	        	}
		     
		 return min;
		 }
}
