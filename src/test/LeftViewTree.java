package test;

import java.util.HashMap;
import java.util.Scanner;

// A Binary Tree node
class Node
		{
		    int data;
		    Node left, right;
		    Node(int item)
		    {
		        data = item;
		        left = right = null;
		    }
		}
public class LeftViewTree{
		    public static void main(String args[])
		    {
		        Scanner sc = new Scanner(System.in);
		        int t = sc.nextInt();
				//Node root=null;
		        while (t > 0)
		        {
		            HashMap<Integer, Node> m = new HashMap<Integer, Node> ();
		            int n = sc.nextInt();
		            Node root=null;
		            while (n > 0)
		            {
		                int n1 = sc.nextInt();
		                int n2 = sc.nextInt();
		                char lr = sc.next().charAt(0);
		                Node parent = m.get(n1);
		                if (parent == null)
		                {
		                    parent = new Node(n1);
		                    m.put(n1, parent);
		                    if (root == null)
		                        root = parent;
		                }
		                Node child = new Node(n2);
		                if (lr == 'L')
		                    parent.left = child;
		                else
		                    parent.right = child;
		                m.put(n2, child);
		                n--;
		            }
		            Tree g = new Tree();
					g.leftView(root);
					System.out.println();
		         t--;	
		        }
		    }
		}
		/*This is a function problem.You only need to complete the function given below*/
		/* A Binary Tree node
		class Node
		{
		    int data;
		    Node left, right;
		    Node(int item)
		    {
		        data = item;
		        left = right = null;
		    }
		}*/
		class Tree
		{
		    static int plevel = 1;
		    void leftView(Node root)
		    {
		      // Your code here
		     int h = getheight(root);
		     System.out.println(h);
		      for(int i =1;i<=h;i++){
		          plevel =1;
		          printleft(root,i);
		      }

		    }
		    
		   void printleft(Node root, int h){
		    if(root == null){
		          return;
		      }
		     if(h == 1 && plevel == 1){
		        System.out.print(root.data+ " ");
		        plevel = 0;
		     return;
		     }
		     printleft(root.left,h-1);
		     printleft(root.right,h-1);
		         
		    }
		    
		    int getheight(Node root){
		    	 if (root == null) 
		             return 0; 
		         else 
		         { 
		   
		    
		             /* use the larger one */
		             return max(getheight(root.left),getheight(root.right))+1;
		         
		   }
		 }

			private int max(int h1, int h2) {
				// TODO Auto-generated method stub
				return h1>h2 ? h1 :h2;
			}
		}
