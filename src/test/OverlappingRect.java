package test;

import java.util.Scanner;

public class OverlappingRect {

		public static void main (String[] args)
		 {
		 //code
		 Scanner sc = new Scanner(System.in);
	        int t = sc.nextInt();
	        while (t-- > 0)
	        {
	             int x1 = sc.nextInt();
			   int y1 = sc.nextInt();
			    int x2 = sc.nextInt();
			   int y2 = sc.nextInt();
			   System.out.println("\n");
	            int x3 = sc.nextInt();
			   int y3 = sc.nextInt();
			    int x4 = sc.nextInt();
			   int y4 = sc.nextInt();
			   System.out.println(testOverlap(x1,x2,y1,y2,x3,x4,y3,y4));
			   
	        }
		 }
		public static int testOverlap(int x1,int x2,int y1,int y2,int x3,int x4,int y3,int y4){
		    if(x1 > x4 || x2 < x3){
			       return 0;
			   }
			   if(y1 > y4 || y2 < y3){
			       	return 0;

			   }
			   return 1;
		}
	
}
